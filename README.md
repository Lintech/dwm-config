### What's inside this dwm?
+ 2 patches, viz: [**activetagindicatorbar**](https://dwm.suckless.org/patches/activetagindicatorbar/) [**systray**](https://dwm.suckless.org/patches/systray/)
+ Modified version of **dwm 6.2** assembled **from 6.4**
+ Customizable tray (lacks keyboard layout)
+ Gruvbox gtk and qt5, qt6 theme ready config
+ You do not need to enter startx to log in if you have **shell zsh** (you can remove startx in **.zprofile**)
+ Ready-to-use hotkeys for some applications
+ ~~Used [**Ly - a TUI display manager**](https://github.com/fairyglade/ly)~~

## Command
```bash
sudo pacman -S noto-fonts-cjk noto-fonts-emoji ttf-font-awesome ttf-roboto kvantum alacritty lxappearance zsh xclip flameshot numlockx mesa-utils dunst feh
sudo pacman -S pacman -S --needed git wget curl base-devel
git clone https://aur.archlinux.org/yay-bin.git
cd yay-bin
makepkg -si
yay -S betterlockscreen nerd-fonts-jetbrains-mono ttf-unifont gruvbox-dark-gtk gruvbox-material-icon-theme-git
chmod +x ~/.xinitrc
sh -c "$(curl -fsSL https://raw.githubusercontent.com/romkatv/zsh4humans/v5/install)"
```

How do I add patches for dwm?
```bash
git add patch.diff
sudo make clean install 
```
if you want to put sddm together with dwm
```bash
chmod +x sddm-dwm-inst.sh
wget https://codeberg.org/Lintech/dwm-config/raw/commit/368dcf383c769ff526b985948aca9e30ac17953b/sddm-dwm-inst.sh
```

## Screnhoot
## New dwm 6.2
![scren3](image/scren3.png)
![scren4](image/scren4.png)
## old dwm
![scren1](image/scren1.png)
![scren2](image/scren2.png)