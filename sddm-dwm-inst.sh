#!/bin/bash

mkdir /home/$USER/.tempsddm && cd /home/$USER/.tempsddm
wget https://codeberg.org/Lintech/dwm-config/raw/commit/da3ccf45602b1639a1d3e948adbed823bf65003e/dwm.desktop

FILE=/usr/share/xsessions
if [ -d "$FILE" ]; then
    sudo mv dwm.desktop /usr/share/xsessions/
    else
    sudo mkdir /usr/share/xsessions/
    sudo mv dwm.desktop /usr/share/xsessions/
fi

cd ..
rm -rf .tempsddm